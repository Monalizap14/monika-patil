#include<stdio.h>
int gcd(int a,int b);
int main()
{
	/* code */
    int a,b;
    printf("Enter value of a and b :");
    scanf("%d%d",&a,&b);
    printf("gcd of %d and %d = %d",a,b,gcd(a,b));
	return 0;
}
int gcd(int a,int b)
{
   if(a==0)
   	  return b;
   if(b==0)
   	  return a;
   if(a==b)
   	  return a;
   if(a>b)
   	  return (gcd(a-b,b));
   return (gcd(a,b-a));
}